package com.example.calculator;

import static java.lang.Integer.parseInt;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class MainFragment extends Fragment {


    private EditText calculation,result;
    private String curr,res,operator;
    private Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnAC,btnDel,btnDivision,btnMultiplication,btnSubtraction,btnAddition,btnPoint,btnEqualsTo;
    private boolean dotInserted, operatorInserted;
    private float leftOperand, rightOperand;

    View view;

    public MainFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void displayCalculation(){
        calculation.setText(curr);
    }

    public void displayResult(){
        result.setText(res);
    }

    public void clearAll(){
        curr = "";
        res = "";
        dotInserted = false;
        operatorInserted = false;
    }

    public void delete(){
        if(curr.isEmpty()) return;
        else {
            // if last character is a dot
            if(curr.charAt(curr.length()-1) == '.'){
                dotInserted = false;
            }

            // if last character is a operator
            if(curr.charAt(curr.length()-1) == ' ') {
                curr = curr.substring(0,curr.length()-3);
                operatorInserted = false;
                return;
            }
            curr = curr.substring(0,curr.length()-1);
        }
    }

    public void compute(){
        // if operator is not present or curr is empty or last character is dot or last character is an operator, return;
        if(!operatorInserted || curr.charAt(curr.length()-1) == '.' || curr.isEmpty() || curr.charAt(curr.length()-1) == ' ') return;

        String operandsAndOperator[] = curr.split(" ");
        leftOperand = Float.valueOf(operandsAndOperator[0]);
        rightOperand = Float.valueOf(operandsAndOperator[2]);
        operator = operandsAndOperator[1];
        float tempRes = 0;

        switch(operator){
            case "+":
                tempRes = leftOperand + rightOperand;
                break;

            case "-":
                tempRes = leftOperand - rightOperand;
                break;

            case "*":
                tempRes = leftOperand * rightOperand;
                break;

            case "/":
                tempRes = leftOperand / rightOperand;
                break;

            default:
                break;
        }

        res = String.valueOf(tempRes);
        int indexOfDot = res.indexOf('.');
        int numberAfterDecimal = parseInt(res.substring(indexOfDot+1));
        if(numberAfterDecimal == 0) res = res.substring(0,indexOfDot);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main, container, false);

        calculation = (EditText) view.findViewById(R.id.calculation);
        result = (EditText) view.findViewById(R.id.result);

        curr = "";
        res = "";
        dotInserted = false;
        operatorInserted = false;
        leftOperand = 0;
        rightOperand = 0;
        operator = "";

        btn0 = (Button) view.findViewById(R.id.btn0);
        btn1 = (Button) view.findViewById(R.id.btn1);
        btn2 = (Button) view.findViewById(R.id.btn2);
        btn3 = (Button) view.findViewById(R.id.btn3);
        btn4 = (Button) view.findViewById(R.id.btn4);
        btn5 = (Button) view.findViewById(R.id.btn5);
        btn6 = (Button) view.findViewById(R.id.btn6);
        btn7 = (Button) view.findViewById(R.id.btn7);
        btn8 = (Button) view.findViewById(R.id.btn8);
        btn9 = (Button) view.findViewById(R.id.btn9);
        btnAC = (Button) view.findViewById(R.id.btnAC);
        btnDel = (Button) view.findViewById(R.id.btnDel);
        btnDivision = (Button) view.findViewById(R.id.btnDivision);
        btnMultiplication = (Button) view.findViewById(R.id.btnMultiplication);
        btnSubtraction = (Button) view.findViewById(R.id.btnSubtraction);
        btnAddition = (Button) view.findViewById(R.id.btnAddition);
        btnPoint = (Button) view.findViewById(R.id.btnPoint);
        btnEqualsTo = (Button) view.findViewById(R.id.btnEqualsTo);

        btn0.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "0";
                displayCalculation();
            }
        });

        btn1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "1";
                displayCalculation();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "2";
                displayCalculation();
            }
        });

        btn3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "3";
                displayCalculation();
            }
        });

        btn4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "4";
                displayCalculation();
            }
        });

        btn5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "5";
                displayCalculation();
            }
        });

        btn6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "6";
                displayCalculation();
            }
        });

        btn7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "7";
                displayCalculation();
            }
        });

        btn8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "8";
                displayCalculation();
            }
        });

        btn9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                curr = curr + "9";
                displayCalculation();
            }
        });

        btnPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking if text field is empty
                if(curr.isEmpty()){
                    curr = "0.";
                    dotInserted = true;
                } else {
                    // checking if dot is already present;
                    if(dotInserted){
                        return;
                    } else {
                        curr = curr + ".";
                        dotInserted = true;
                    }
                }

                // checking if last character is operator
                if(curr.substring(curr.length()-1) == " ") {
                    System.out.println("reached");
                    curr = curr + "0.";
                    dotInserted = true;
                }
                displayCalculation();
            }
        });

        btnAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAll();
                displayCalculation();
                displayResult();
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete();
                displayCalculation();
            }
        });

        btnDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(curr.isEmpty()) return;
                else {
                    // if last digit is dot, then return
                    if(curr.charAt(curr.length()-1) == '.') return;

                    // if an operator is already present, then return
                    if(operatorInserted) return;

                    curr = curr + " / ";
                    operatorInserted = true;
                    displayCalculation();
                }
            }
        });

        btnMultiplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(curr.isEmpty()) return;
                else {
                    // if last digit is dot, then return
                    if(curr.charAt(curr.length()-1) == '.') return;

                    // if an operator is already present, then return
                    if(operatorInserted) return;

                    curr = curr + " * ";
                    operatorInserted = true;
                    displayCalculation();
                }
            }
        });

        btnSubtraction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(curr.isEmpty()) return;
                else {
                    // if last digit is dot, then return
                    if(curr.charAt(curr.length()-1) == '.') return;

                    // if an operator is already present, then return
                    if(operatorInserted) return;

                    curr = curr + " - ";
                    operatorInserted = true;
                    displayCalculation();
                }
            }
        });

        btnAddition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(curr.isEmpty()) return;
                else {
                    // if last digit is dot, then return
                    if(curr.charAt(curr.length()-1) == '.') return;

                    // if an operator is already present, then return
                    if(operatorInserted) return;

                    curr = curr + " + ";
                    operatorInserted = true;
                    displayCalculation();
                }
            }
        });

        btnEqualsTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                displayResult();
            }
        });

        return view;
    }
}